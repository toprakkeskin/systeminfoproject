﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WUApiLib;

namespace SystemInfoClientService.SystemInfoHelpers
{
	class UpdateInfo
	{

		public UpdateInfo()
		{
		}


		public static List<String> GetUpdates()
		{
			UpdateSession UpdateSession = new UpdateSession();
			IUpdateSearcher UpdateSearchResult = UpdateSession.CreateUpdateSearcher();
			UpdateSearchResult.Online = true;
			ISearchResult SearchResults = UpdateSearchResult.Search("IsInstalled=0 AND IsPresent=0");
			List<String> updateList = new List<String>();
			
			foreach (IUpdate x in SearchResults.Updates)
			{
				updateList.Add(x.Title);
			}
			return updateList;
		}
	}
}
