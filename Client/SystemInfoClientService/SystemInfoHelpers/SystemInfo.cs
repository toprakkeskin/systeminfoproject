﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace SystemInfoClientService.SystemInfoHelpers
{
	public struct SysInfo
	{
		public string machineName;
		public int cpuUsage;
		public int availableMem;
		public int totalMem;
		public List<string> updateList;
		public SysInfo(string machineName, int cpuUsage, int availableMem, int totalMem, List<string> updateList)
		{
			this.machineName = machineName;
			this.cpuUsage = cpuUsage;
			this.availableMem = availableMem;
			this.totalMem = totalMem;
			this.updateList = updateList;
		}
	};

	sealed class SystemInfo
	{
		private static SystemInfo _inst = null;
		private static readonly object padlock = new object();

		private PerformanceCounter _cpuCounter = null;
	

		private SystemInfo()
		{
		}

		public SysInfo GetSystemInfo()
		{
			if(_cpuCounter == null)
				_cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");

			

			// Temp System Info Struct Instance
			SysInfo tempInfo = new SysInfo();

			// Get machine name
			tempInfo.machineName = System.Environment.MachineName;

			// Take a sample
			CounterSample cs1 = _cpuCounter.NextSample();
			// Sleep for a while
			System.Threading.Thread.Sleep(500);
			// Take another sample
			CounterSample cs2 = _cpuCounter.NextSample();

			
			// Set values
			tempInfo.cpuUsage = (int)CounterSample.Calculate(cs1,cs2);
			tempInfo.availableMem = (int)MemoryStatus.GetPhysicalAvailableMemoryInMiB();
			tempInfo.totalMem = (int)MemoryStatus.GetTotalMemoryInMiB();
			tempInfo.updateList = UpdateInfo.GetUpdates();



			return tempInfo;
		}

		public static SystemInfo Instance
		{
			get
			{
				if (_inst == null)
				{
					lock (padlock)
					{
						_inst = new SystemInfo();
					}
				}
				return _inst;
			}
		}

	}

	
}

