﻿using SystemInfoClientService.Service;
using SystemInfoClientService.SystemInfoHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SystemInfoClientService
{
	class Program
	{
		static void Main(string[] args)
		{
		


			try
			{

				var rc = HostFactory.Run(x =>
				{
					x.Service<ServiceFunctionality>(s =>
					{
						//bu değerler argüman olarak alınacak
						s.ConstructUsing(name => new ServiceFunctionality(new System.Net.IPAddress(new byte[] { 127, 0, 0, 1 }), 4000, "b_serv1"));
						s.WhenStarted(tc => tc.Start());
						s.WhenStopped(tc => tc.Stop());
					});
					x.RunAsLocalSystem();

					x.SetDescription("ITMS HW");
					x.SetDisplayName("ITMSService");
					x.SetServiceName("ITMSService");
				});
				var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
				Environment.ExitCode = exitCode;
			}
			catch (Exception ex)
			{

				throw;
			}






		}
	}
}
