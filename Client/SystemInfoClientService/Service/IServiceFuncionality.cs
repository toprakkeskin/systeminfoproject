﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemInfoClientService.Service
{
	interface IServiceFuncionality
	{
		Task Start();
		Task Stop();
	}
}
