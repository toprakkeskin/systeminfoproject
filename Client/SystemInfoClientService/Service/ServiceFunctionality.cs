﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SystemInfoClientService.SystemInfoHelpers;

namespace SystemInfoClientService.Service
{
	class ServiceFunctionality: IServiceFuncionality
	{

		int port;
		IPAddress ipAddr;
		string lab;

		private Settings _settings;

		public ServiceFunctionality(IPAddress ip, int port, string lab)
		{
			ipAddr = ip;
			this.port = port;
			this.lab = lab;
			this._settings = new Settings();
			this._settings.UpdateInterval = 10;
		}



		public async Task Start()
		{
			while (true)
			{

				Console.WriteLine("============================================");
				Console.WriteLine("========= Get System Informations ==========");
				SystemInfo t = SystemInfo.Instance;
				SysInfo sysInfo = t.GetSystemInfo();

				Console.WriteLine("-- Machine Name :" + sysInfo.machineName);

				float percentFree = ((float)sysInfo.availableMem / (float)sysInfo.totalMem) * 100;
				float percentOccupied = 100 - percentFree;
				
				Console.WriteLine($"-- Number of awaiting updates :{sysInfo.updateList.Count} ");

				foreach (String updateTitle in sysInfo.updateList)
				{
					Console.WriteLine($"-- Available update : {updateTitle}");
				}


				Console.WriteLine("-- CPU Usage (%) " + sysInfo.cpuUsage.ToString());
				Console.WriteLine("-- Available Physical Memory (MiB) " + sysInfo.availableMem.ToString());
				Console.WriteLine("-- Total Memory (MiB) " + sysInfo.totalMem.ToString());
				Console.WriteLine("-- Free (%) " + percentFree.ToString());
				Console.WriteLine("-- Occupied (%) " + percentOccupied.ToString());

				string output = JsonConvert.SerializeObject(sysInfo);
				Console.WriteLine("-- Serialized JSON: " + output);

				HttpClient client = new HttpClient();

				string serverUrl = "http://127.0.0.1:2710/api/service";
				client.DefaultRequestHeaders.Clear();
				client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				// client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}")));

				StringContent jsonContent = new StringContent(output, Encoding.UTF8);
				jsonContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				Console.WriteLine("=============== Data Sending ===============");

				HttpResponseMessage response = await client.PostAsync(serverUrl, jsonContent);
				response.EnsureSuccessStatusCode();

				Console.WriteLine("=============== Data Sended ================");

				string responseBody = await response.Content.ReadAsStringAsync();
				Console.WriteLine("-- Response Code:" + response.StatusCode);
				Console.WriteLine("-- Response Content:" + responseBody);

				Settings updatedSettings = JsonConvert.DeserializeObject<Settings>(responseBody);
				Console.WriteLine("-- Updated Settings-> UpdateInverval: " + updatedSettings.UpdateInterval.ToString());

				_settings.UpdateInterval = updatedSettings.UpdateInterval;

				Console.WriteLine("============= Settings Updated =============");
				Console.WriteLine("============================================\n");

				Console.WriteLine("-- Sleep " +  _settings.UpdateInterval + " Seconds \n");
				Thread.Sleep(_settings.UpdateInterval * 1000);
			}
		}



		public async Task Stop()
		{
			Console.WriteLine("Service has stopped!");
		}
	}
}
