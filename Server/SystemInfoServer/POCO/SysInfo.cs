﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystemInfoServer.POCO
{
	public class SysInfo 
	{
		public string time { get; set; }
		
		public string machineName { get; set; }
		public int cpuUsage { get; set; }
		public int availableMem { get; set; }
		public int totalMem { get; set; }
		public List<string> updateList { get; set; }
	


	}
}
