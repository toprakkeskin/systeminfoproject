﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystemInfoServer.Model.Dto
{
	public class LoginDto
	{
		public string uname { get; set; }
		public string pass { get; set; }
	}
}
