﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SystemInfoServer.Model
{
    public class Login 
    {
		public int id { get; set; }
		public string uname { get; set; }
		public string pass { get; set; }
    }
}
