﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using SystemInfoServer.Model;
using SystemInfoServer.POCO;

namespace SystemInfoServer
{
    public class Startup
    {
		List<Login> loginDb;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
			loginDb = new List<Login>()
			{
				new Login(){uname = "toprakkeskin" , pass= "12345" }
			};
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
			services.AddLogging();
			services.AddSingleton(new List<SysInfo>());
			services.AddSingleton(loginDb);
			services.AddCors();

		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
			app.UseCors(builder =>
			{
				builder.AllowAnyOrigin();
				builder.AllowAnyMethod();
				builder.AllowAnyHeader();
			});
		}
    }
}
