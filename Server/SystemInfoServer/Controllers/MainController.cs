﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SystemInfoServer.Model;
using SystemInfoServer.Model.Dto;
using SystemInfoServer.POCO;

namespace SystemInfoServer.Controllers
{

    [Produces("application/json")]
    [Route("api/")]
    public class MainController : Controller
    {
		private static int UpdateInterval = 0;
		List<SysInfo> sysInfoList; //sunucuya gelen servislerin listesi
		private ILogger logger; //
		List<Login> loginDb;
		public MainController(ILoggerFactory logger,List<SysInfo> sysInfoList, List<Login> loginDb)
		{
			this.logger = logger.CreateLogger("Trace");
			this.sysInfoList = sysInfoList;
			this.loginDb = loginDb;
		}
        
        [HttpPost("service")]
        public ActionResult Post([FromBody]SysInfo value)
        {
			logger.LogWarning($"Yeni servis geldi {value.machineName}");
			value.time = $"{DateTime.Now.Hour}:{DateTime.Now.Minute}";
			sysInfoList.Add(value);
			return Json(new Settings() { UpdateInterval = UpdateInterval });		
        }
		[HttpPost("login")]
		public ActionResult LoginCall([FromBody]LoginDto login)
		{
			if (ModelState.IsValid)
			{
				//bellek db'sinde ilgili kullanıcı var mı??
				var res = loginDb.Where((i) => i.uname == login.uname && i.pass == login.pass);
				if (res.Count() == 0)
				{
					logger.LogWarning("Yanlış kullanıcı adı şifre");
					return Content("0"); 
				}
				else
				{
					logger.LogWarning($"Başarılı kullanıcı girişi : {login.uname}");
					return Content("1"); 
					
				}
			}
			else
				return BadRequest();
		}

		[HttpPost("interval")]
		public ActionResult SetInterval( [FromBody] Settings settings)
		{
			if (ModelState.IsValid)
			{
				UpdateInterval = settings.UpdateInterval;
				logger.LogWarning($"Interval belirlendi : {settings.UpdateInterval}");
				return Content("1");
			}
			else
				return Content("0");
		}
		[HttpGet("fetch/all")]
		public ActionResult FetchService()
		{
			logger.LogWarning("Tüm makinelerin kayıtları istendi");
			return Json(sysInfoList.TakeLast(20));
		}
		[HttpGet("fetch/{name}")]
		public ActionResult FetchServiceById(string name)
		{
			logger.LogWarning($"{name} isimli makinenin datası istendi");
			var res = sysInfoList.Where((i) => i.machineName == name);
			return Json(res);
		}
		[HttpGet("search/{keyword}")]
		public ActionResult SearchMachineByName(string keyword)
		{
			

			logger.LogWarning($"{keyword} aratildi.");
			
			return Json(sysInfoList.Where(i => i.machineName.Contains(keyword)).Select(i => i.machineName).ToHashSet<string>());
		}


	}
}
